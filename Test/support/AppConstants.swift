//
//  AppConstants.swift
//  Test
//
//  Created by Fourth Dev on 05/11/2021.
//

import Foundation

class AppConstants {
    static let listProjects = "listProjects.json"
    static let didLoadProjects = "didLoadProjects"
    static let endPoint = "https://tapuniverse.com"
}
