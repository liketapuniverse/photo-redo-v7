//
//  ListProjects.swift
//  Test
//
//  Created by Fourth Dev on 05/11/2021.
//

import Foundation
import Alamofire
import SVProgressHUD

class ListProject {
    static var shared = ListProject()
    var projects: [Project] = []
    init() {
       loadProjects()
    }
    
    func loadProjects() {
        if let listCodableProjects = readSavedProjects() {
            projects = listCodableProjects.map(){Project(id:$0.id, name: $0.name)}
            NotificationCenter.default.post(name: NSNotification.Name(AppConstants.didLoadProjects), object: nil)
        } else {
            SVProgressHUD.show()
            AF.request("\(AppConstants.endPoint)/xproject").responseJSON { response in
                guard let res = response.value as? [String: Any],
                      let pros = res["projects"] as? [[String: Any]] else {return}
                for project in pros {
                    guard let id = project["id"] as? Int,
                          let name = project["name"] as? String else { continue }
                    self.projects.append(Project(id: String(id), name: name))
                }
                NotificationCenter.default.post(name: NSNotification.Name(AppConstants.didLoadProjects), object: nil)
            }
        }
    }
    
    func saveProjects() {
        var listCodableProjects: [CodableProject] = []
        listCodableProjects = projects.map() {CodableProject(id: $0.id, name: $0.name)}
        guard let data = try? JSONEncoder().encode(listCodableProjects),
              let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        else { return }
        let encodedString = String(data: data, encoding: .utf8)
        let path = url.appendingPathComponent(AppConstants.listProjects)
        do {
            try encodedString?.write(to: path, atomically: false, encoding: .utf8)
        } catch {
            return
        }
    }
    
    func readSavedProjects()-> [CodableProject]? {
        guard let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil}
        let path = url.appendingPathComponent(AppConstants.listProjects)
        guard let string = try? String(contentsOf: path) else {return nil}
        let listProjects = try? JSONDecoder().decode([CodableProject].self, from: Data(string.utf8))
        return listProjects
    }
}
