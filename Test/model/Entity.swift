//
//  Entity.swift
//  Test
//
//  Created by Fourth Dev on 05/11/2021.
//

import UIKit

class CodableProject: Codable {
    let id: String
    let name: String
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}

class Photo {
    let id: String
    let image: UIImage
    let frame: CGRect
    var transform: CGAffineTransform
    var alpha: Double
    init(id: String, image: UIImage, frame: CGRect, transform: CGAffineTransform, alpha: Double) {
        self.id = id
        self.image = image
        self.frame = frame
        self.transform = transform
        self.alpha = alpha
    }
    
    func save(_ image: CustomImageView) {
        transform = image.transform
        alpha = image.alpha
    }
    
    func load(codablePhoto: CodablePhoto, path: URL)-> Photo? {
        return nil
    }
}

class CodablePhoto: Codable {
    let id: String
    let frame: CGRect
    let transform: CGAffineTransform
    let alpha: Double
    init(id: String, frame: CGRect, transform: CGAffineTransform, alpha: Double) {
        self.id = id
        self.frame = frame
        self.transform = transform
        self.alpha = alpha
    }
}
