//
//  ProjectDetail.swift
//  Test
//
//  Created by Fourth Dev on 05/11/2021.
//

import Foundation
import Alamofire
import SVProgressHUD

class Project {
    let id: String
    let name: String
    var photos: [Photo] = []
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    func checkSavedPhotos(completion: @escaping (Photo)-> Void) {
        photos = []
        if UserDefaults.standard.bool(forKey: "didCache-\(id)") {
            guard let listCodablePhotos = readPhotos(),
                  let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                    else {
                        getPhotosFromApi(completion: completion)
                        return
                    }
            for codablePhoto in listCodablePhotos {
                let path = url.appendingPathComponent("photo-\(codablePhoto.id).jpg")
                guard let data = try? Data(contentsOf: path),
                      let image = UIImage(data: data)
                    else { continue }
                let photo = Photo(id: codablePhoto.id, image: image, frame: codablePhoto.frame, transform: codablePhoto.transform, alpha: codablePhoto.alpha)
                photos.append(photo)
                completion(photo)
            }
        }  else {
            getPhotosFromApi(completion: completion)
        }
    }
    
    func getPhotosFromApi(completion: @escaping (Photo)-> Void) {
        SVProgressHUD.show()
        AF.request("\(AppConstants.endPoint)/xprojectdetail", method: .post, parameters: ["id":Int(id)]).responseJSON { response in
            guard let res = response.value as? [String: Any],
                  let resPhotos = res["photos"] as? [[String: Any]]
            else { return }
            var count = 0
            for resPhoto in resPhotos {
                guard let url = resPhoto["url"] as? String,
                      let frame = resPhoto["frame"] as? [String: Any],
                      let x = frame["x"] as? Double,
                      let y = frame["y"] as? Double,
                      let width = frame["width"] as? Double,
                      let height = frame["height"] as? Double
                else {
                    count += 1
                    continue
                }
                AF.request(url).responseData { response in
                    if let data = response.data {
                        guard let image = UIImage(data: data) else { return }
                        let photo = Photo(id: UUID().uuidString, image: image, frame: CGRect(x: x, y: y, width: width, height: height), transform: .identity, alpha: 1)
                        self.photos.append(photo)
                        completion(photo)
                    }
                }
                count += 1
                if count == resPhoto.count {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    func saveProject() {
        guard let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let listCodablePhotos: [CodablePhoto] = photos.map(){CodablePhoto(id: $0.id, frame: $0.frame, transform: $0.transform, alpha: $0.alpha)}
        guard let encodedString = try? JSONEncoder().encode(listCodablePhotos) else { return }
        let projectPath = url.appendingPathComponent("project-\(id).json")
        try? encodedString.write(to: projectPath)
        for photo in photos {
            let path = url.appendingPathComponent("photo-\(photo.id).jpg")
            guard let data = photo.image.jpegData(compressionQuality: 1) else {
                continue
                
            }
            if !FileManager.default.fileExists(atPath: path.absoluteString) {
                try? data.write(to: path)
            }
        }
        UserDefaults.standard.set(true, forKey: "didCache-\(id)")
    }
    
    func readPhotos()-> [CodablePhoto]? {
        guard let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        let path = url.appendingPathComponent("project-\(id).json")
        guard let data = try? Data(contentsOf: path),
              let listCodablePhotos = try? JSONDecoder().decode([CodablePhoto].self, from: data) else {
                  return nil
                  
              }
        return listCodablePhotos 
    }
}
