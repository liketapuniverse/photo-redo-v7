//
//  GalleryView.swift
//  Test
//
//  Created by Fourth Dev on 05/11/2021.
//

import UIKit
protocol GalleryViewDelegate: AnyObject {
    func imageExported(_ image: UIImage)
    func imageSelected(_ image: CustomImageView)
}

class GalleryView: UIView {
    var project: Project!
    let deleteButton = CustomButton()
    var currentZoomScale: Double = 1
    
    let line = CAShapeLayer()
    let circle = CAShapeLayer()
    let circleRadius: Double = 6
    var listId: [String] = []
    var selected: Int = -1
    var currentImageView: CustomImageView? = nil
    var didLoad = false
    weak var delegate: GalleryViewDelegate?
    override func draw(_ rect: CGRect) {
        if didLoad { return}
        setUpDeleteButton()
        setLayerHidden(true)
        didLoad = true
    }
    
    private func setUpDeleteButton() {
        let radius: Double = 20
        deleteButton.frame = CGRect(x: 0, y: 0, width: radius, height: radius)
        deleteButton.backgroundColor = .red
        deleteButton.layer.cornerRadius = radius/2
        let centerWhiteView = UIView()
        centerWhiteView.frame = CGRect(x: 0, y: 0, width: 8, height: 2)
        centerWhiteView.backgroundColor = .white
        centerWhiteView.center = CGPoint(x: deleteButton.bounds.midX, y: deleteButton.bounds.midY)
        deleteButton.addSubview(centerWhiteView)
        addSubview(deleteButton)
        deleteButton.isHidden = true
    }
    
    func presentPhoto(_ photo: Photo) {
        let image = CustomImageView(image: photo.image)
        image.id = photo.id
        image.frame = photo.frame
        image.transform = photo.transform
        addSubview(image)
        setUpImageView(image)
        listId.append(photo.id)
    }
    
    func addPhoto(_ image: UIImage) {
        let pickedImage = CustomImageView(image: image)
        pickedImage.id = UUID().uuidString
        var width = self.bounds.width/2
        var height = width*(pickedImage.bounds.height/pickedImage.bounds.width)
        if height > self.bounds.height {
            let downSizeRatio  = self.bounds.height/height
            height *= downSizeRatio
            width *= downSizeRatio
        }
        pickedImage.frame = CGRect(x: 0, y: 0, width: width, height: height)
        pickedImage.center = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
        addSubview(pickedImage)
        setUpImageView(pickedImage)
        let photo = Photo(id: pickedImage.id, image: image, frame: pickedImage.frame, transform: .identity, alpha: 1)
        project.photos.append(photo)
        listId.append(photo.id)
    }
    
    func updateOpacity(_ alpha: Double) {
        guard let image = currentImageView else { return }
        image.alpha = alpha
        project.photos[selected].save(image)
    }
    
    func exportPhoto() {
        deleteButton.isHidden = true
        if let delegate = delegate {
            let image = drawImageFromSize(CGSize(width: bounds.width, height: bounds.height))
            delegate.imageExported(image)
        }
    }
    
    private func drawImageFromSize(_ size: CGSize) -> UIImage {
        let format = UIGraphicsImageRendererFormat()
        let renderer = UIGraphicsImageRenderer(size: size, format: format)
        let image = renderer.image { ctx in
            ctx.cgContext.setFillColor((UIColor(named: "galleryBackground") ?? UIColor.white).cgColor)
            ctx.fill(CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height))
            var newPhotos: [Photo] = []
            for child in self.subviews {
                if let photo = child as? CustomImageView {
                    project.photos.forEach { oldPhoto in
                        if oldPhoto.image == photo.image {
                            newPhotos.append(oldPhoto)
                        }
                    }
                }
            }
            for photo in newPhotos {
                let t = photo.transform.concatenating(CGAffineTransform(translationX: photo.frame.midX, y: photo.frame.midY))
                let rect = CGRect(x: -photo.frame.width/2, y: -photo.frame.height/2, width: photo.frame.width, height: photo.frame.height)
                ctx.cgContext.concatenate(t)
                photo.image.draw(in: rect, blendMode: .normal, alpha: photo.alpha)
                ctx.cgContext.concatenate(t.inverted())
            }
        }
        return image
    }
}

//MARK: Image View Gesture

extension GalleryView {
    func setUpImageView(_ image: CustomImageView) {
        image.isUserInteractionEnabled = true
        image.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTap(_:))))
        image.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(didPan(_:))))
        image.addGestureRecognizer(UIPinchGestureRecognizer(target: self, action: #selector(didPinch(_:))))
        image.addGestureRecognizer(UIRotationGestureRecognizer(target: self, action: #selector(didRotate(_:))))
    }
    
    func notifyChange(_ image: CustomImageView) {
        updateDeleteButtonFrame(image)
        drawForImage(image)
        if let i = listId.firstIndex(where: { id in
            id == image.id
        }) {
            selected = i
            project.photos[i].save(image)
        }
        bringSubviewToFront(image)
        currentImageView = image
        if let d = delegate {
            d.imageSelected(image)
        }
    }
    
    func updateDeleteButtonFrame(_ image: CustomImageView) {
        let imageTransform = image.transform
        let scale = sqrt(pow(imageTransform.a, 2) + pow(imageTransform.c, 2))*currentZoomScale
        deleteButton.isHidden = false
        deleteButton.center = image.convert(CGPoint(x: image.bounds.midX, y: -15/scale), to: self)
        deleteButton.transform = self.transform.inverted()
    }
    
    @objc func didTap(_ g: UITapGestureRecognizer) {
        guard let image = g.view as? CustomImageView else { return }
        notifyChange(image)
    }
    
    @objc func didPan(_ g: UIPanGestureRecognizer) {
        guard let image = g.view as? CustomImageView else { return }
        if g.state == .began {
            image.beginT = image.transform
        }
        if g.state == .changed {
            let translation = g.translation(in: self)
            image.transform = image.beginT.translatedBy(x: translation.x, y: translation.y)
        }
        notifyChange(image)
    }
    
    @objc func didPinch(_ g: UIPinchGestureRecognizer) {
        guard let image = g.view as? CustomImageView else { return }
        if g.state == .began {
            image.beginT = image.transform
        }
        if g.state == .changed {
            image.transform = image.beginT.scaledBy(x: g.scale, y: g.scale)
        }
        notifyChange(image)
    }
    
    @objc func didRotate(_ g: UIRotationGestureRecognizer) {
        guard let image = g.view as? CustomImageView else { return }
        if g.state == .began {
            image.beginT = image.transform
        }
        if g.state == .changed {
            image.transform = image.beginT.rotated(by: g.rotation)
        }
        notifyChange(image)
    }
    
    func reDrawViewInScrollView() {
        if let view = currentImageView {
            updateDeleteButtonFrame(view)
            drawForImage(view)
        }
    }
    
    
}

//MARK: Border For Image

extension GalleryView {
    func drawForImage(_ image: CustomImageView) {
        setLayerHidden(false)
        let topLeft = image.convert(CGPoint(x: 0, y: 0), to: self)
        let topRight = image.convert(CGPoint(x: image.bounds.width, y: 0), to: self)
        let bottomRight = image.convert(CGPoint(x: image.bounds.width, y: image.bounds.height), to: self)
        let bottomLeft = image.convert(CGPoint(x: 0, y: image.bounds.height), to: self)
        
        let linePath = UIBezierPath()
        linePath.move(to: topLeft)
        linePath.addLine(to: topRight)
        linePath.addLine(to: bottomRight)
        linePath.addLine(to: bottomLeft)
        linePath.close()
        line.path = linePath.cgPath
        line.fillColor = UIColor.clear.cgColor
        line.strokeColor = UIColor.blue.cgColor
        line.lineWidth = 2/currentZoomScale
        
        let circlePath = UIBezierPath()
        for p in [topLeft, topRight, bottomRight, bottomLeft] {
            circlePath.append(UIBezierPath(arcCenter: p, radius: circleRadius/currentZoomScale, startAngle: 0, endAngle: .pi*2, clockwise: true))
        }
        circle.path = circlePath.cgPath
        circle.fillColor = UIColor.blue.cgColor
        layer.addSublayer(line)
        layer.addSublayer(circle)
    }
    
    func setLayerHidden(_ hidden: Bool) {
        line.isHidden = hidden
        circle.isHidden = hidden
    }
}

class CustomImageView: UIImageView {
    var id: String = ""
    var beginT: CGAffineTransform = .identity
}

class CustomButton: UIButton {
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return bounds.insetBy(dx: -10, dy: -10).contains(point)
    }
}
