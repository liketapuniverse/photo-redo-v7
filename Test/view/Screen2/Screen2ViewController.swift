//
//  Screen2ViewController.swift
//  Test
//
//  Created by Fourth Dev on 05/11/2021.
//

import UIKit
import Photos

class Screen2ViewController: UIViewController {
    var project: Project!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var galleryView: GalleryView!
    @IBOutlet weak var sliderView: SliderView!
    var tempImage: UIImage? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        galleryView.clipsToBounds = true
        galleryView.delegate = self
        sliderView.delegate = self
        sliderView.isHidden = true
        sliderView.delegate = self
        project.checkSavedPhotos { photo in
            self.galleryView.project = self.project
            self.galleryView.presentPhoto(photo)
        }
    }
    
    @objc func didTapOutsideGallery() {
        galleryView.deleteButton.isHidden = true
        galleryView.setLayerHidden(true)
    }
    
    private func alertOnPermissionDenied() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Allow access", message: "Please allow acess to photo so we can save new photo for you", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default) { _ in
                guard let url = URL(string: UIApplication.openSettingsURLString),
                      UIApplication.shared.canOpenURL(url) else {
                          assertionFailure("Unable to open app setting")
                          return
                      }
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Save project", message: "Save changes to this project?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Don't save", style: .cancel) { _ in
                self.navigationController?.popViewController(animated: true)
            }
            let saveAction = UIAlertAction(title: "Save", style: .default) { _ in
                self.project.saveProject()
                self.navigationController?.popViewController(animated: true)
            }
            alert.addAction(cancelAction)
            alert.addAction(saveAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func exportPhoto(_ sender: Any) {
        let status = PHPhotoLibrary.authorizationStatus()
        if status == .authorized {
            self.galleryView.exportPhoto()
        }  else if status == .denied {
            alertOnPermissionDenied()
        } else if status == .notDetermined {
            PHPhotoLibrary.requestAuthorization { newStatus in
                if newStatus == .authorized {
                    self.galleryView.exportPhoto()
                } else {
                    self.alertOnPermissionDenied()
                }
            }
        }
    }
    @IBAction func addPhoto(_ sender: Any) {
        guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Screen3ViewController") as? Screen3ViewController else { return }
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
}

extension Screen2ViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return galleryView
    }
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        reloadView(scrollView)
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        reloadView(scrollView)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        reloadView(scrollView)
    }
    
    func reloadView(_ scrollView: UIScrollView) {
        galleryView.currentZoomScale = scrollView.zoomScale
    }
}


extension Screen2ViewController: CustomPhotoPickerDelegate {
    func imageDidSelected(_ image: UIImage) {
        galleryView.addPhoto(image)
    }
}

extension Screen2ViewController: SliderViewDelegate {
    func alphaChanged(_ alpha: Double) {
        galleryView.updateOpacity(alpha)
    }
}

extension Screen2ViewController: GalleryViewDelegate {
    func imageSelected(_ image: CustomImageView) {
        sliderView.isHidden = false
        sliderView.value = image.alpha
    }
    
    func imageExported(_ image: UIImage) {
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.doneSavingImage(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @objc func doneSavingImage(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Error", message: "Error exporting photo", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            tempImage = image
            DispatchQueue.main.async {
                let ac = UIAlertController(title: "Saved!", message: "Photo was saved successfully", preferredStyle: .alert)
                let shareAction = UIAlertAction(title: "Share now", style: .default) { _ in
                    if let newImage = self.tempImage {
                        self.shareImage(newImage)
                    }
                }
                ac.addAction(UIAlertAction(title: "OK", style: .default))
                ac.addAction(shareAction)
                self.present(ac, animated: true)
            }
        }
    }
    
    private func shareImage(_ image: UIImage) {
        let activityViewController = UIActivityViewController(activityItems: [image], applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
}
