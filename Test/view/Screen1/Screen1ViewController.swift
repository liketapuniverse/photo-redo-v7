//
//  ViewController.swift
//  Test
//
//  Created by Fourth Dev on 05/11/2021.
//

import UIKit
import SVProgressHUD

class Screen1ViewController: UIViewController {
    var model = ListProject.shared
    @IBOutlet weak var projectTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(forName: Notification.Name(AppConstants.didLoadProjects), object: nil, queue: nil) { [weak self] _ in
            DispatchQueue.main.async {
                self?.projectTableView.reloadData()
                SVProgressHUD.dismiss()
            }
        }
    }
     
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    @IBAction func addProject(_ sender: Any) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Add project", message: nil, preferredStyle: .alert)
            alert.addTextField { textField in
                textField.placeholder = "Enter project name"
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let OkAction = UIAlertAction(title: "Ok", style: .default) { _ in
                guard let textField = alert.textFields?.first else { return }
                if textField.text == "" {
                    let noEmptyAlert = UIAlertController(title: "Empty text", message: "Please enter a name", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    noEmptyAlert.addAction(okAction)
                    self.present(noEmptyAlert, animated: true, completion: nil)
                } else {
                    guard let text = textField.text else { return }
                    let project = Project(id: UUID().uuidString, name: text)
                    self.model.projects.append(project)
                    self.projectTableView.reloadData()
                    self.model.saveProjects()
                }
            }
            alert.addAction(cancelAction)
            alert.addAction(OkAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension Screen1ViewController: UITableViewDelegate, UITableViewDataSource, ProjectTableViewCellDelegate {
    func deleteProject(_ cell: ProjectTableViewCell) {
        guard let indexPath = projectTableView.indexPath(for: cell) else { return }
        model.projects.remove(at: indexPath.row)
        projectTableView.reloadData()
        self.model.saveProjects()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.projects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProjectTableViewCell") as? ProjectTableViewCell else { return UITableViewCell()}
        cell.projectNameLabel.text = model.projects[indexPath.row].name
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Screen2ViewController") as? Screen2ViewController else {return}
        vc.project = model.projects[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
