//
//  ProjectTableViewCell.swift
//  Test
//
//  Created by Fourth Dev on 05/11/2021.
//

import UIKit
protocol ProjectTableViewCellDelegate: AnyObject {
    func deleteProject(_ cell: ProjectTableViewCell)
}

class ProjectTableViewCell: UITableViewCell {
    var beginX: Double = 0
    @IBOutlet weak var projectNameLabel: UILabel!
    weak var delegate: ProjectTableViewCellDelegate?
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    var didLoad = false

    override func draw(_ rect: CGRect) {
        trailingConstraint.constant = 0
        if didLoad { return }
        let pan = UIPanGestureRecognizer(target: self, action: #selector(didPan(_:)))
        pan.delegate = self
        addGestureRecognizer(pan)
        didLoad = true
    }
    
    override func prepareForReuse() {
        trailingConstraint.constant = 0
    }
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard let pan = gestureRecognizer as? UIPanGestureRecognizer else { return false}
        let p =  pan.velocity(in: self)
        return abs(p.x) > abs(p.y)
    }
    
    @IBAction func deleteProject(_ sender: Any) {
        if let d = delegate {
            d.deleteProject(self)
        }
    }
    @objc func didPan(_ g: UIPanGestureRecognizer) {
        if g.state == .began {
            beginX = trailingConstraint.constant
        }
        if g.state == .changed {
            let translation = g.translation(in: self)
            trailingConstraint.constant = beginX - translation.x
        }
        if g.state == .ended {
            let currentTranslation = trailingConstraint.constant
            var limitedTranslation: Double
            limitedTranslation = max(currentTranslation, 0)
            limitedTranslation = min(currentTranslation, 50)
            limitedTranslation = limitedTranslation<20 ? 0 : 50
            trailingConstraint.constant = limitedTranslation
            UIView.animate(withDuration: 0.2) {
                self.layoutIfNeeded()
            }

        }
    }
}
